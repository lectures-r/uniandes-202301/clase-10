---
pagetitle: "10-Bucles en R"
title: "Taller de R: Estadística y programación"
subtitle: "Clase 10: Bucles en R"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [ECON-1302](https://bloqueneon.uniandes.edu.co/d2l/home/247211)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: darkblue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,tidyverse,janitor,rio,skimr,kableExtra,data.table)
Sys.setlocale("LC_CTYPE", "en_US.UTF-8") # Encoding UTF-8

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)

# generate data
df = data.frame(cod_mpio = c(5001,5002,5003,5004,5005,5006),	
                violencia_2014	= c(0.05,0.07,0.06,0.03,0.04,0.03),
                violencia_2015	= c(0.09,0.05,0.03,0.06,0.03,0.01),
                violencia_2016	= c(0.02,0.04,0.03,0.02,0.03,0.00),
                violencia_2017	= c(0.03,0.06,0.03,0.01,0.04,0.01),
                violencia_2018	= c(0.01,0.02,0.04,0.05,0.07,0.01),
                violencia_2019  = c(0.01,0.02,0.02,0.03,0.03,0.01))
set.seed(12345)

```

<!--=================-->
<!--=================-->

## **[0.] Checklist**

Antes de iniciar con esta lectura asegúrese de...

#### **☑ Lectures previas**

Asegúrese de haber revisado la [Clase 9:Visualización de información: Aplicación - GEIH](https://lectures-r.gitlab.io/uniandes-202301/clase-09/)

#### **☑ Script de la clase:**

Puede bajar el script y los conjuntos de datos de la clase [aquí](https://github.com/taller-r-202301/clase-010/archive/refs/heads/main.zip)

#### **☑ Versión de R**

Tener la versión `r R.version.string` instalada:

```{r, cache=F , echo=T}
R.version.string
```

#### **☑ Librerías**

Instale/llame la librería `pacman`, y use la función `p_load()` para instalar/llamar las librerías de esta sesión:

```{r eval=T}
# llamar/instalar librería pacman
if(!require(pacman)){install.packages("pacman") ; require(pacman)}  

# llamar y/o instalar las librerías de la clase
p_load(tidyverse,rio) 
# rio: para importar archivos desde diferentes formatos 
# tydiverse: conjunto de librerías para manipular dataframes
```

<!------------------------------>
<!-------- Estructura ---------->         
<!------------------------------>
## 1. Bucles o ciclos

### ¿que es un bucle o ciclo?

Un bucle o loops, en programación, es una secuencia de instrucciones de código que se ejecuta repetidas veces, hasta que la condición asignada a dicho bucle deja de cumplirse. Los tres bucles más utilizados en programación son el bucle while, el bucle for y el bucle do-while. [Wikipedia](https://es.wikipedia.org/wiki/Bucle_(programación)
 
<!------------------------------> 

### Estructura de un bucle (for, while y repeat)

<center>
<img src="http://community.datacamp.com.s3.amazonaws.com/community/production/ckeditor_assets/pictures/132/content_flowchart1.png" height=500>
</center>

<!------------------------------>
### Estructura de un bucle (for)

```{r,eval=T}
vector = 1:5
for (i in vector){ # Vector sobre el que se va a aplicar el loop
     i = i*i  # Sobreescribe i como el resultado de i*i
     print(i) # Pinta el resultado sobre la consola
}
```


<mark>`vector`</mark> contiene los elementos sobre los que se va a iterar la sentencia del loop. 

<mark>`{}`</mark> contiene la sentencia o acciones que se ejecutan dentro del loop.

<mark>`i`</mark> Es el objeto local del loop este asume un valor diferente del <mark>`vector`</mark> en cada iteración. Cuando termina de ejecutarse la sentencia dentro de <mark>`{}`</mark>, el loop pasará al siguiente elemento del vector y así hasta que se haya ejecutado la sentencia sobre todos los elementos del <mark>`vector`</mark>.


<!------------------------------>
### Estructura de un bucle (repeat)

```{r,eval=T}
set.seed(0117) # fijar semil
repeat{
       m = rnorm(n=1 , mean=10 , sd=2) # generar un número aleatorio (media 10 , sd 2) 
       print(m) # pintar el número sobre la consola
       if (m <= 8){ # condicionar a que ese número sea menor o igual a 8
                   break # detener el loop si m es menor o igual a 8 
       } 
}
```

<mark>`m`</mark> es el objeto local de este bucle. En cada iteración, el objeto `m` se va a sobrescribir hasta que termine de ejecutarse el loop.

<mark>`(m <= 8)`</mark> contiene la condición que se debe cumplir para que se pueda continuar ejecutando el loop.

<mark>`{}`</mark> contiene la sentencia o acciones que se ejecutan dentro del loop.

<!------------------------------>
### Estructura de un bucle (while)

```{r,eval=T}
j = 1

while (j <= 5) { # condición
        print(j*j) # pintar sobre la consola el producto de j*j
        j = j+1 # sobreescribir j como j + 1
}
```

<mark>`j`</mark> es el objeto local de este bucle. En cada iteración, el objeto `j` se va a sobrescribir hasta que termine de ejecutarse el loop.

<mark>`()`</mark> contiene la condición que se debe cumplir para que se pueda ejecutar la sentencia.

<mark>`{}`</mark> contiene la sentencia o acciones que se ejecutan dentro del loop.

**Nota:** cada acción que se quiera ejecutar en la sentencia se debe escribir en una linea de código diferente (a menos que se use el operador `%>%` o el operador ` ; `). 

<!------------------------------>
<!---------- Ejemplos ---------->         
<!------------------------------>

### Veamos un ejemplo (...)

Tenemos una base de datos llamada `df`, que contiene 7 columnas con el código DANE de municipio y el porcentaje de la población que ha sufrido violencia entre 2014 y 2019. Ahora vamos a usar un loop para reemplazar cada columna por el valor en `%` (multiplicar cada columna por 100).

<center>
```{r,echo=F}
kbl(df) %>% kable_paper() %>% scroll_box(width = "800px", height = "300px")
```
</center>

<!------------------------------>
### Veamos un ejemplo (...)

El loop se aplica desde la columna 2 (porque la columna 1 contiene el código DANE) hasta la última columna del dataframe. 

```{r,eval=T,include=T,echo=T}
for(i in 2:ncol(df)){
    df[,i] <- df[,i]*100
}
```

![](public/pics/loop.gif){width=70%}


<!----------------------------------------------------------------------------->
<!---------------------------- Controles de flujo ----------------------------->
<!----------------------------------------------------------------------------->

##  2. Controles de flujo (if, else, next, breack) 
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!------------------------------>
### condicionales

Los condicionales <mark>`if`</mark> y <mark>`else`</mark> los usamos para definir una condición dentro de un lopp, la cual define cuando se debe o no, aplicar la sentencia dentro de `{}`.

<!------------------------------>
### condicionales: if

```{r,eval=T}
number = seq(from=1, to=6 , by=1)
pares = seq(from=2 , to=6 , by=2)

for (j in number) { # Vector sobre el que se va a aplicar el loop

       if (j %in% pares){ # condición para if
             print(paste0(j," - es un # par")) # cuando se cumple la condición
       }
}
```

La función <mark>`if`</mark> debe ir acompañada por una condición definida entre paréntesis <mark>`()`</mark> y seguidamente se debe definir entre <mark>`{}`</mark> la sentencia que se va a aplicar cuando se cumpla la condición. 

**Nota:** para los valores `j` que no son pares, el loop no aplica ninguna sentencia. Es decir, como no definimos que hacer cuando el objeto `j` no sea par, el loop pasa al otro elemento de `number` sin aplicar ninguna acción sobre `j`.

<!------------------------------>
### condicionales: if y else

```{r,eval=T}
for (j in number) { # Vector con los numeros del 1 al 7

       if (j %in% pares){ # condición para if
             print(paste0(j," - es un # par")) # cuando se cumple la condición
       }
  
       else { # Cuando no se cumple la condición de if entonces...
             print(paste0(j," - NO es un # par"))
       }
}
```

Ahora hemos definido dentro del loop que hacer cuando no se cumpla la condición que se definió para `if`.

**Nota:** un loop puede tener más una condición `if` de control del cuerpo del loop, pero solo puede haber maximo un `else` por cada `if`. 

<!------------------------------>
### condicionales: next y break

```{r,eval=T}
for (j in number) { # Vector con los numeros del 1 al 7

       if (j > 5) {
           break
       }
       if (j %in% pares){ # condición
             print(paste0(j," - es un # par")) # cuando se cumple la condición
       }
       else {
             next 
       }
}
```

Se usa <mark>`next`</mark> dentro de sentencia para indicar al loop que debe saltar al siguiente elemento de `number`. 

Se usa <mark>`break`</mark> dentro de sentencia para indicar al loop que debe detenerse. 

<!--=================-->
<!--=================-->
## **[3.] Apply, Lapply & Sapply**

![](pics/familia_apply.png){width=80%}

### **3.1 Apply**
![](pics/apply.png){width=80%}

```{r}
####  Operaciones por columnas
mtcars
apply(X = mtcars, MARGIN = 2, FUN = min)
apply(mtcars , 2 , function(columna) min(...=columna , na.rm=T)) 

####  Operaciones por filas
apply(X = mtcars, MARGIN = 1, function(x) sum(x))
```

### **3.2 Lapply**
![](pics/lapply.png){width=80%}

```{r}
## aplicar sobre dataframe
lapply(mtcars, function(x) summary(x))
lap <- lapply(mtcars, function(x) summary(x))
lap


storms
table(is.na(storms$hu_diameter))
table(is.na(storms$ts_diameter))
lapply(storms ,function(x)  table(is.na(x)))
```

### **3.3 Sapply**
![](pics/sapply.png){width=80%}
```{r}
sap <- sapply(mtcars, summary)
sap
```

## **[4.] Aplicación: chip**

```{r , message=F , eval=F}
## limpiar entorno
rm(list=ls()) 

## Chip
browseURL("https://www.chip.gov.co/schip_rt/index.jsf")

## 1. Obtener rutas de los datos
list.files("input/chip",full.names=T, recursive = T) 
paths <- list.files("input/chip",full.names=T, recursive = TRUE) %>% unlist()

## 2. Hacer ejemplo para una observacion

## 2.1. leer archivo
data <- import("input/chip/2019/11767600044K212410-1220191625694914330.xls") %>% as_tibble() 

## 2.2. obtener codigo-DANE 
name_mpio = colnames(data)[1]

## 2.3. obtener tipo de inversion
tipo = data[8,2]

## 2.4. obtener valor
valor = data[8,8]

## 2.5. consolidar informacion
df = tibble(name=name_mpio , tipo_inver = tipo , valor_inv = valor)

## 3. Generalizar ejemplo en una función
f_extrac <- function(ruta){
  
  ## 2.1. leer archivo
  data = import(ruta)
  
  ## 2.2. obtener codigo-DANE 
  name_mpio = colnames(data)[1]
  
  ## 2.3. obtener tipo de inversion
  tipo = data[8,2]
  
  ## 2.4. obtener valor
  valor = data[8,8]
  
  ## 2.5. consolidar informacion
  df = tibble(name=name_mpio , tipo_inver = tipo , valor_inv = valor)
            
  ## 3.6 Retornar output
  return(df)
}

## 3.1 Apliquemos la función generalizada.

data_20 = f_extrac(paths[20])
data_30 = f_extrac(paths[30])

# 3.2 Apliquems lapply
data_lapply = lapply(paths, function(x) f_extrac(ruta = x))

# 3.3 Juntemos todo en un solo dataframe
data_df = rbindlist(l = data_lapply , use.names = T)
print(data_df)
```

<!--------------------->
## **Para seguir leyendo:**

* Wickham, Hadley and Grolemund, Garrett, 2017. R for Data Science [[Ver aquí]](https://r4ds.had.co.nz)

  + Cap. 5: Data transformation
  + Cap. 10: Tibbles
  + Cap. 12: Tidy data

